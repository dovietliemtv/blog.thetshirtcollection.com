<?php

$id = get_the_ID();

$chosen_sidebar = get_post_meta(get_the_ID(), "qode_show-sidebar", true);
$default_array = array('default', '');

if(!in_array($chosen_sidebar, $default_array)){
	$sidebar = get_post_meta(get_the_ID(), "qode_show-sidebar", true);
}else{
	$sidebar = $qode_options_proya['blog_single_sidebar'];
}

$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments']))
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}
$single_type = qode_get_meta_field_intersect('blog_single_type');
$single_loop = 'blog_single';
$single_grid = 'yes';
$single_class = array('blog_single', 'blog_holder');
if($single_type == 'image-title-post'){
	$single_loop = 'blog-single-image-title-post';
	$single_grid = 'no';
	$single_class[] = 'single_image_title_post';
}
?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
		var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>


	<?php get_template_part( 'title' ); ?>
<div class="container_inner" style="max-width: 800px; margin: 0 auto 20px auto">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Archive - 1 (blog.thetshirtcollection.com) -->
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-4120324712409592"
         data-ad-slot="8905887865"
         data-ad-format="auto"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>

	<?php get_template_part( 'slider' ); ?>

				<?php if($single_type == 'image-title-post') : //this post type is full width ?>
					<div class="full_width" <?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
						<?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
							<div class="overlapping_content"><div class="overlapping_content_inner">
						<?php } ?>
						<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>
				<?php else : // post type ?>
					<div class="container"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
						<?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
							<div class="overlapping_content"><div class="overlapping_content_inner">
						<?php } ?>
								<div class="container_inner default_template_holder" <?php qode_inline_style($content_style_spacing); ?>>
				<?php endif; // post type end ?>
					<?php if(($sidebar == "default")||($sidebar == "")) : ?>
						<div <?php qode_class_attribute(implode(' ', $single_class)) ?>>
                            <h1>hello</h1>
						<?php
                        get_template_part('templates/' . $single_loop, 'loop');
						?>
						<?php if($single_grid == 'no'): ?>
							<div class="grid_section">
								<div class="section_inner">
                                    <?php endif; ?>
							<?php
								if($blog_hide_comments != "yes"){
									comments_template('', true);
								}else{
									echo "<br/><br/>";
								}
							?>
						<?php if($single_grid == 'no'): ?>
								</div>
							</div>
						<?php endif; ?>
                        </div>

                    <?php elseif($sidebar == "1" || $sidebar == "2"): ?>
						<?php if($sidebar == "1") : ?>	
							<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1">
						<?php elseif($sidebar == "2") : ?>	
							<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
								<div class="column1">
						<?php endif; ?>
					
									<div class="column_inner">
										<div <?php qode_class_attribute(implode(' ', $single_class)) ?>>
											<?php
											get_template_part('templates/' . $single_loop, 'loop');
											?>
										</div>
										
										<?php
											if($blog_hide_comments != "yes"){
												comments_template('', true);
											}else{
												echo "<br/><br/>";
											}
                                        ?>
                                        <div class="pre_next_link">
                                            <div class="custom_link previous_post_link">
                                                <p>Previous article</p>
                                                <?php  previous_post_link(); ?>
                                            </div>
                                            <div class="custom_link next_post_link">
                                                <p>Next article</p>
                                                <?php   next_post_link(); ?>
                                            </div>
                                        </div>

                                        <div style="display: table; clear: both; width: 100%; min-height: 60px; margin-top: 60px">
                                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                            <!-- Single post - 2 (blog.thetshirtcollection.com) -->
                                            <ins class="adsbygoogle"
                                                 style="display:block"
                                                 data-ad-client="ca-pub-4120324712409592"
                                                 data-ad-slot="2998955065"
                                                 data-ad-format="auto"></ins>
                                            <script>
                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                            </script>
                                        </div>

                                        <div class="follow_sns_overflow">
                                            <div class="follow_sns">
                                                <div class="text_sns_page btn_sns_page">
                                                    <h5>Follow us: </h5>
                                                </div>
                                                <div class="fb_button btn_sns_page">
                                                    <a href="https://www.facebook.com/tcanimeblog">
                                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                                        <span>Facebook</span>
                                                    </a>
                                                </div>

                                                <div class="in_button btn_sns_page">
                                                    <a href="https://www.instagram.com/the_tshirt_collection/">
                                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                                        <span>Instagram</span>
                                                    </a>
                                                </div>
                                                <div class="tw_button btn_sns_page">
                                                    <a href="https://twitter.com/TshirtCollect">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                        <span>Twitter</span>
                                                    </a>
                                                </div>

                                                <div class="p_button btn_sns_page">
                                                    <a href="https://www.pinterest.com/tcinter/">
                                                        <i class="fa fa-pinterest" aria-hidden="true"></i>
                                                        <span>Pinterest</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ads_top_relate">
                                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                            <!-- Single post - 3 (mobile, blog.thetshirtcollection.com) -->
                                            <ins class="adsbygoogle"
                                                 style="display:block"
                                                 data-ad-client="ca-pub-4120324712409592"
                                                 data-ad-slot="1051217064"
                                                 data-ad-format="auto"></ins>
                                            <script>
                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                            </script>
                                        </div>



                                        <?php
                                        echo do_shortcode('[related_posts_by_tax format="thumbnails" image_size="medium" posts_per_page="3"]');
										?>
                                    </div>
                                    <div class="fb-comments" data-href="<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>"></div>

                                </div>
								<div class="column2"> 
									<?php get_sidebar(); ?>
								</div>
							</div>
						<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
							<?php if($sidebar == "3") : ?>	
								<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"> 
									<?php get_sidebar(); ?>
								</div>
								<div class="column2">
							<?php elseif($sidebar == "4") : ?>	
								<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
									<div class="column1"> 
										<?php get_sidebar(); ?>
									</div>
									<div class="column2">
							<?php endif; ?>
							
										<div class="column_inner">
                                            <div <?php qode_class_attribute(implode(' ', $single_class)) ?>>
												<?php
												get_template_part('templates/' . $single_loop, 'loop');
												?>
											</div>
											<?php
												if($blog_hide_comments != "yes"){
													comments_template('', true); 
												}else{
													echo "<br/><br/>";
												}
											?> 
										</div>
									</div>	
									
								</div>
						<?php endif; ?>
					</div>
                <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                    </div></div>
                <?php } ?>
                 </div>



<?php endwhile; ?>
<?php endif; ?>



<?php get_footer(); ?>	